import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kised/controllers/device_info_controller.dart';
import 'package:kised/widgets/avatar_video_player.dart';
import 'package:kised/widgets/background_stopped_image.dart';
import 'package:kised/widgets/menus/menu.dart';
import 'package:kised/widgets/record_button.dart';
import 'package:kised/widgets/setting_btn.dart';
import '../widgets/current_time.dart';
import '../controllers/port_state_controller.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  @override
  void initState() {
    Get.put(PortStateController()).getPorts();
    Get.put(DeviceInfoController()).onInit();

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    Get.put(PortStateController()).connectTo(null);
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: Stack(
            children: [
              BackgroundStoppedImage(),
              AvatarVideoPlayer(),
              Positioned(
                top: height * 0.02083,  // 80
                left: width * 0.03704,  // 80
                child: SizedBox(
                  height: height * 0.088802083, // 341
                  width: width * 0.22222, // 480
                  child: SvgPicture.asset(
                    "assets/img/kisedLogo.svg",
                    color: Colors.white,
                  ),
                 ),
              ),
              Positioned(
                right: width * 0.03704,
                top: height * 0.0174479167,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: const [
                    CurrentTime(),
                  ],
                ),
              ),
              GetBuilder<PortStateController>(
                  init: PortStateController(),
                  builder: (controller) => !controller.isDetected?MainMenu():Container() // 근접센서 !
              ),
              SettingBtn(),
              // stt button
              // Container(
              //   width: width*0.4,
              //   height: height*0.2,
              //   child: RecordButton(),
              // ),
              Positioned(
                right: height * 0.02083,  // 80
                bottom: width * 0.03704,  // 80
                child: SizedBox(
                  height: height * 0.02083, // 80
                  width: width * 0.18519, // 400
                  child: SvgPicture.asset(
                    "assets/img/stpLogo.svg",
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
