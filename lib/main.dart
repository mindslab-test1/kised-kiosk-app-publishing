import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kised/routes.dart';

void main() =>
    {runApp(GetMaterialApp(initialRoute: "/SplashScreen", getPages: routes))};
