import 'dart:async';
import 'dart:typed_data';

import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:kised/controllers/video_state_controller.dart';
import 'package:usb_serial/transaction.dart';
import 'package:usb_serial/usb_serial.dart';

class PortStateController extends GetxController {
  bool _isDetected = false;

  bool get isDetected => _isDetected;

  UsbPort? _port;
  List<Widget> _ports = [];
  final List<Widget> _serialData = [];

  StreamSubscription<String>? _subscription;
  Transaction<String>? _transaction;
  UsbDevice? _device;

  Future<bool> connectTo(UsbDevice? device) async {
    _serialData.clear();

    if (_subscription != null) {
      _subscription!.cancel();
      _subscription = null;
    }

    if (_transaction != null) {
      _transaction!.dispose();
      _transaction = null;
    }

    if (_port != null) {
      _port!.close();
      _port = null;
    }

    if (device == null) {
      _device = null;
      return true;
    }

    _port = await device.create();
    if (await (_port!.open()) != true) {
      return false;
    }
    _device = device;

    await _port!.setDTR(true);
    await _port!.setRTS(true);
    //TODO Change PORT HERE
    await _port!.setPortParameters(
        9600, UsbPort.DATABITS_8, UsbPort.STOPBITS_1, UsbPort.PARITY_NONE);

    _transaction = Transaction.stringTerminated(
        _port!.inputStream as Stream<Uint8List>, Uint8List.fromList([13, 10]));

    _subscription = _transaction!.stream.listen((String line) {
      // sensor recognized
      if (line == "yes") {
        if (!_isDetected && Get.put(VideoStateController()).videoUrl!='assets/video/hello.mp4') {
          Get.put(VideoStateController())
              .changeUrl("https://media.maum.ai/media/space-s/video/spaceS043.mp4",isChange: true, changingUrl: "assets/video/wait_left.mp4");
          _isDetected = !_isDetected;
        }
      }
      _serialData.add(Text(line));
      if (_serialData.length > 20) {
        _serialData.removeAt(0);
      }
      update();
    });
    return true;
  }

  void getPorts() async {
    _ports = [];
    List<UsbDevice> devices = await UsbSerial.listDevices();
    if (!devices.contains(_device)) {
      connectTo(null);
    }

    for (var device in devices) {
      // * Connect Device if arduino
      if (device.productName == null) {
        connectTo(_device == device ? null : device);
      }

      _ports.add(ListTile(
          leading: Icon(Icons.usb),
          title: Text(device.productName ?? "null"),
          subtitle: Text(device.manufacturerName ?? 'null'),
          trailing: ElevatedButton(
            child: Text(_device == device ? "Disconnect" : "Connect"),
            onPressed: () {
              connectTo(_device == device ? null : device).then((res) {
                getPorts();
              });
            },
          )));
    }
    update();
  }

  void setIsDetected(bool isDetected) {
    _isDetected = isDetected;
  }
}
