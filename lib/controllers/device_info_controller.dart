import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class DeviceInfoController extends GetxController {
  String _identifier = '';

  String get identifier => _identifier;

  @override
  void onInit() async {
    super.onInit();
    final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
    try{
      if(Platform.isAndroid) {
        var data = await deviceInfoPlugin.androidInfo;
        _identifier = data.androidId;
      } else if (Platform.isIOS) {
        var data = await deviceInfoPlugin.iosInfo;
        _identifier = data.identifierForVendor;
      }
    } on PlatformException catch (err) {
      print(err);
    }
  }
}