import 'dart:async';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:google_speech/google_speech.dart';
import 'package:kised/repository/repository.dart';
import 'package:rxdart/rxdart.dart';
import 'package:sound_stream/sound_stream.dart';
import 'package:kised/utils/debouncer.dart';

class GoogleSttController extends GetxController {
  final RecorderStream _recorder = RecorderStream();
  var recognizing = false.obs;
  var recognizeFinished = false.obs;
  String text = '';
  String name = '';
  bool isShowAudioBox = false;
  StreamSubscription<List<int>>? _audioStreamSubscription;
  BehaviorSubject<List<int>>? _audioStream;
  Timer? _startTimer;
  Debouncer debouncer = Debouncer(milliseconds: 1000);

  @override
  void onInit() {
    super.onInit();
    init();
  }

  void init() {
    _recorder.initialize();
  }

  void toggleRecordStatus(bool status) {
    recognizing.value = status;
    update();
  }

  void toggleRecognizeFinished(bool status) {
    recognizeFinished.value = status;
    update();
  }

  void toggleShowAudioBox(bool status) {
    isShowAudioBox = status;
    update();
  }

  void hideAudioBox() {
    isShowAudioBox = false;
    update();
  }

  void setText(String txt){
    text = txt;
    update();
  }

  Future<void> toggleRecord(bool isSending) async {
    print('###################');

    if(recognizing.value) {
      await stopRecording(isSending);
    } else {
      await streamingRecognize();
    }
  }

  Future<void> streamingRecognize({Function? callback, bool isSending = true}) async {
    print('##### stream start');

    if(!recognizing.value){
      _audioStream = BehaviorSubject<List<int>>();
      _audioStreamSubscription = _recorder.audioStream.listen((event) {
        if(!_audioStream!.isClosed) _audioStream!.add(event);
      });

      await _recorder.start();

      if(_startTimer != null){
        _startTimer!.cancel();
      }

      _startTimer = Timer(Duration(milliseconds: 6500), () async{
        print('####timer end');
        await _recorder.stop();
        await _audioStreamSubscription?.cancel();
        await _audioStream?.close();
        toggleShowAudioBox(false);
        toggleRecordStatus(false);

        _startTimer!.cancel();
      });

      toggleRecordStatus(true);

      final serviceAccount = ServiceAccount.fromString(
          (await rootBundle.loadString('assets/google_stt/lustrous-center-329507-5ba42add9479.json')));
      final speechToText = SpeechToText.viaServiceAccount(serviceAccount);
      final config = _getConfig();

      final responseStream = speechToText.streamingRecognize(
          StreamingRecognitionConfig(config: config, interimResults: true,),
          _audioStream!);

      var responseText = '';

      toggleShowAudioBox(true);

      responseStream.listen((data) {
        print('##### timer end ################ $data');


        final currentText = data.results.map((e) => e.alternatives.first.transcript).join('\n');
        _startTimer!.cancel();

        if(data.results.first.isFinal) {
          toggleRecognizeFinished(true);
        } else {
          if(currentText.trim() != '' && currentText.trim() != null && currentText.trim() != '\n') {
            print(responseText);
            print(currentText);
            setText((responseText + '\n' + currentText).replaceAll(".", "").replaceAll("\n", " "));
            toggleRecognizeFinished(true);

            debouncer.run(() async {
              await stopRecording(isSending);
              if(callback != null) callback();
              setText('');
            });
          }
        }
      }, onDone: (){
        toggleRecordStatus(false);
      });
    }
  }

  Future<void> stopRecording(bool isSending) async {
    if(!recognizing.value) return;
    await _recorder.stop();
    await _audioStreamSubscription?.cancel();
    await _audioStream?.close();
    toggleShowAudioBox(false);
    toggleRecordStatus(false);
    if(isSending){
      await Repository.sendTextReq(text);
    }
    else {
      name = text.split(" ")[0].replaceAll("\n", "");
    }

  }

  Future<void> stopWithoutSendStt() async{
    await _recorder.stop();
    await _audioStreamSubscription?.cancel();
    await _audioStream?.close();
    toggleShowAudioBox(false);
    toggleRecordStatus(false);
  }

  RecognitionConfig _getConfig() => RecognitionConfig(
      encoding: AudioEncoding.LINEAR16,
      model: RecognitionModel.basic,
      enableAutomaticPunctuation: true,
      sampleRateHertz: 16000,
      languageCode: 'ko-KR');

  Future<void> goHome() async {
    toggleShowAudioBox(false);
    toggleRecordStatus(false);
    await _recorder.stop();
    await _audioStreamSubscription?.cancel();
    await _audioStream?.close();

    streamingRecognize();
  }
}