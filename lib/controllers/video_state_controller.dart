import 'package:async/async.dart';
import 'package:get/get.dart';
import 'package:video_player/video_player.dart';
import 'package:flutter/material.dart';

class VideoStateController extends GetxController {
  String _videoUrl = "";
  VideoPlayerController? _controller;
  VideoPlayerController? get controller => _controller;

  String get videoUrl => _videoUrl;

  bool isLoopUrl(String url) {
    return url=="assets/video/wait_center.mp4" || url=="assets/video/wait_left.mp4";
  }

  bool isWaitUrl(String url) {
    return url!="assets/video/wait_center.mp4" && url!="assets/video/wait_left.mp4";
  }

  bool isNetwork(String url) {
    return url.startsWith("http");
  }

  bool isCenterUrl(){
    return _videoUrl=="assets/video/wait_center.mp4"|| _videoUrl=="assets/video/hello.mp4";
  }

  Future<void> changeUrl(String url, {bool isChange = false, String changingUrl = ""}) async {
    _videoUrl = url;
    if (_controller == null) {
      isNetwork(url)?_initNetworkControllers(url, isChange: isChange, changingUrl: changingUrl):_initAssetControllers(url, isChange: isChange, changingUrl: changingUrl);
    } else {
          final oldController = _controller;
          WidgetsBinding.instance!.addPostFrameCallback((_) async {
            await oldController!.dispose();
            isNetwork(url)?_initNetworkControllers(url, isChange: isChange, changingUrl: changingUrl):_initAssetControllers(url, isChange: isChange, changingUrl: changingUrl);
          });
          _controller = null;
          update();
      }
    }


  void _initAssetControllers(String url, {bool isChange = false, String changingUrl = ""}) async {
    _videoUrl = url;
    if(isChange&&changingUrl==""){
      throw Exception("to change url after video, insert after video's url");
    }
    AsyncMemoizer finishVideo = AsyncMemoizer<void>();
    _controller = VideoPlayerController.asset(url)
      ..setLooping(isLoopUrl(url))
      ..initialize().whenComplete(() {
        _controller!.play();
        update();
      })
      ..addListener(() {
        if(_controller != null && isChange){
          if ( _controller!.value.position >= _controller!.value.duration - Duration(milliseconds: 500)) {
            finishVideo.runOnce(() {
              changeUrl(changingUrl);
            });
          }
        }
      });
  }

  void _initNetworkControllers(String url, {bool isChange = false, String changingUrl = ""}) async {
    _videoUrl = url;
    if(isChange&&changingUrl==""){
      throw Exception("to change url after video, insert after video's url");
    }
    AsyncMemoizer finishVideo = AsyncMemoizer<void>();
    _controller = VideoPlayerController.network(url)
      ..setLooping(isLoopUrl(url))
      ..initialize().whenComplete(() {
        _controller!.play();
        update();
      })
      ..addListener(() {
        if(_controller != null && isChange){
          if ( _controller!.value.position >= _controller!.value.duration - Duration(milliseconds: 500)) {
            finishVideo.runOnce(() {
              changeUrl(changingUrl);
            });
          }
        }
      });
  }
}
