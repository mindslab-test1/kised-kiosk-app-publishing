import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:kised/controllers/chat_box_controller.dart';
import 'package:kised/controllers/video_state_controller.dart';

class Repository {
  static Future<void> sendTextReq(String answer) async{
    print(answer);
    Uri uri = Uri.parse('http://serengeti2.maum.ai/api.app/app/v2/handle/catalog/instance/lifecycle/executes');
    String payload = '{"host":"991","session":"11","utter":"$answer"}';
    //String payload = '{"host":"979","session":"Test","utter":"안녕"}';

    var map = <String, dynamic>{};
    map['lifecycleName'] = 'Daejeon-Space-Kiosk-Lifecycle';
    map['payload'] = payload;
    map['target'] = 'SoftwareCatalogInstance';
    map['async'] = 'false';
    map['catalogInstanceName'] = 'Daejeon-Space-Kiosk-Catalog';
    http.Response response = await http.post(
        uri,
        headers: <String, String> {
          'AccessKey' : 'SerengetiAdministrationAccessKey',
          'SecretKey' : 'SerengetiAdministrationSecretKey',
          'LoginId' : 'maum-orchestra-com'
        },
        body: map
    );
    ChatBoxController chatBoxController = Get.put(ChatBoxController());
    print(jsonDecode(utf8.decode(response.bodyBytes).toString().replaceAll("\\\"", "\"").replaceAll("\"{", "{").replaceAll("}\"", "}")));
    Message message = Message.fromJson(jsonDecode(utf8.decode(response.bodyBytes).toString().replaceAll("\\\"", "\"").replaceAll("\"{", "{").replaceAll("}\"", "}")));
    if(response.statusCode == 200 && message.answer != null) {
      chatBoxController.changeChatInfo(
          message.answer!, message.image ?? '', message.expectedIntents ?? []);
      chatBoxController.updateMenu(false);
      if(message.url != null){
        Get.put(VideoStateController()).changeUrl(message.url!, isChange: true, changingUrl: "assets/video/wait_left.mp4");
      }
    }
    chatBoxController.updateRenew(true);
  }
}

class Message {
  final String? answer;
  final String? image;
  final String? url;
  final List<String>? expectedIntents;

  Message({
    this.answer,
    this.image,
    this.url,
    this.expectedIntents
  });

  Message.fromJson(dynamic json)
      : answer = json['answer']['answer']['answer'].toString(),
        image = json['answer']['answer']['image'].toString(),
        url = json['answer']['answer']['url'].toString(),
        expectedIntents = List<String>.from(json['expectedIntents'].map((item) => item['intent'].toString()).toList());
}