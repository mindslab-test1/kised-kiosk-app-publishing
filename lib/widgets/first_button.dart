import 'package:flutter/material.dart';
import 'dart:ui';
import 'package:get/get.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kised/controllers/chat_box_controller.dart';
import 'package:kised/controllers/video_state_controller.dart';

class FirstButton extends StatelessWidget {
  const FirstButton({
    Key? key,
    required this.height,
    required this.width,
  }) : super(key: key);

  final double height;
  final double width;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: height * 0.02083),
        Center(
          child: GestureDetector(
            onTapUp: (TapUpDetails tap){
              Get.put(ChatBoxController()).updateMenu(true);
              Get.put(VideoStateController()).changeUrl("https://media.maum.ai/media/space-s/video/spaceS043.mp4", isChange: true, changingUrl: "assets/video/wait_left.mp4");
            },
            child: Padding(
              padding: EdgeInsets.fromLTRB(width * 0.02963, height * 0.0125,
                  width * 0.02963, height * 0.0125),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(width * 0.02593),
                    child: BackdropFilter(
                      filter: ImageFilter.blur(
                        sigmaX: width * 0.0083,
                        sigmaY: width * 0.0083,
                      ),
                      child: Center(
                        child: Container(
                          width: width * 0.05185,
                          height: width * 0.05185,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                              width: width * 0.000462963,
                              color: Color.fromRGBO(255, 255, 255, 0.25),
                            ),
                            color: Colors.white.withOpacity(0.4),
                          ),
                          child: Center(
                            child: SizedBox(
                              width: width * 0.02963,
                              child: SvgPicture.asset(
                                "assets/img/ico_home_64px.svg",
                                color: Colors.white,
                                fit: BoxFit.fitWidth,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: width * 0.0111),
                  Text(
                    '처음으로',
                    style: TextStyle(
                      fontFamily: 'notoSansKR',
                      fontWeight: FontWeight.w700,
                      color: Colors.white,
                      fontSize: width * 0.02593,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
