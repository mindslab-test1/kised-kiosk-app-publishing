import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class AllSchedule extends StatefulWidget {
  const AllSchedule({Key? key}) : super(key: key);

  @override
  _AllSchedule createState() => _AllSchedule();
}

class _AllSchedule extends State<AllSchedule> {
  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/img/img_timetable.jpg"),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Positioned(
          top: height * 0.145833,  // 560
          right: width * 0.15926, // 344
          child: IconButton(
            icon: SvgPicture.asset(
              "assets/img/ico_close_96px.svg",
              color: Colors.white,
            ),
            iconSize: width * 0.02963,
            onPressed: () {
              // Navigator.pop(context);
            },
          ),
        ),
      ],

    );
  }
}
