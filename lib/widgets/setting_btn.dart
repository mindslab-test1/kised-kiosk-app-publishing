import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'dart:ui';
import '../screens/setting_page.dart';

class SettingBtn extends StatelessWidget {
  const SettingBtn({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    return Positioned(
      bottom: height * 0.02083,  // 80
      left: width * 0.03704,  // 80
      child: ClipRRect(
        borderRadius: BorderRadius.circular(16),
        child:BackdropFilter(
          filter: ImageFilter.blur(
            sigmaX: width*0.00463,
            sigmaY: width*0.00463,
          ),
          child: Center(
            child: Container(
              width: width*0.05185,
              height: width*0.05185,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  width: width*0.000462963,
                  color: Color.fromRGBO(255, 255, 255, 0.25),
                ),
                color: Colors.white.withOpacity(0.4),
              ),
              child: Positioned(
                top: height * 0.02083,  // 80
                right: width * 0.03704,  // 80
                child: IconButton(
                  icon:  SvgPicture.asset(
                    "assets/img/ico_setting_64px.svg",
                    color: Colors.white,
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => SettingPage()),
                    );
                  },
                ),
              ),

            ),
          ),
        ),
      ),
    );
  }
}
