import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kised/controllers/chat_box_controller.dart';
import 'package:kised/controllers/google_stt_controller.dart';

class RecordButton extends StatelessWidget {
  const RecordButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final GoogleSttController googleSttController = Get.put(GoogleSttController());
    final ChatBoxController chatBoxController = Get.put(ChatBoxController());
    googleSttController.onInit();

    return ElevatedButton(
        onPressed: () async {
          await googleSttController.toggleRecord(false);
          //await googleSttController.streamingRecognize();
        //  chatBoxController.updateMenu(false);
        },

        child: GetBuilder<GoogleSttController>(
          init: GoogleSttController(),
          builder: (controller) {
            return Column(
              children: [
                googleSttController.recognizing.value
                    ?Icon(Icons.fiber_manual_record, color: Colors.red, size: 300,)
                    :Icon(Icons.record_voice_over, size: 300,),
                SingleChildScrollView(
                  child: Text(
                    'STT RESULT: ${controller.text}',
                    style: TextStyle(
                      fontSize: 50
                    ),
                  ),
                )
              ],
            );
          },
        ),
        // child: googleSttController.recognizing.value
        //     ?Icon(Icons.fiber_manual_record, color: Colors.red, size: 300,)
        //     :Icon(Icons.record_voice_over, size: 300,)
    );
  }
}
