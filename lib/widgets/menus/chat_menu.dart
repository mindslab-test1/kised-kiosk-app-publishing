import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kised/controllers/chat_box_controller.dart';
import 'package:kised/repository/repository.dart';
import 'package:kised/widgets/first_button.dart';

class ChatMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ChatBoxController chatBoxController = Get.put(ChatBoxController());
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    return Container(
      padding: EdgeInsets.only(right: width * 0.03704),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                SizedBox(
                  height: height * 0.2401,
                ),
                Container(
                  width: width * 0.430556,
                  child: Stack(
                    children: [
                      SizedBox(
                        width: width * 0.430556,
                        child: GetBuilder<ChatBoxController>(
                          builder: (controller) {
                            List<Widget>? listWidget;
                            if (chatBoxController.chats != []) {
                              listWidget = chatBoxController.chats
                                  .map((e) => SideButton(text: e))
                                  .toList();
                            } else {
                              listWidget = null;
                            }

                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  width: width * 0.351852,
                                  child: Text(
                                    chatBoxController.chatTitle,
                                    style: TextStyle(
                                      fontFamily: 'notoSansKR',
                                      fontWeight: FontWeight.w400,
                                      color: Colors.white,
                                      fontSize: width * 0.02407,
                                      // 52
                                      letterSpacing: -1.3,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: height * 0.00625, // 24
                                ),
                                (chatBoxController.imageUrl != '')
                                    ? (chatBoxController.imageUrl.contains("img_space_0")
                                        ? Column(
                                            children: [
                                              SizedBox(
                                                height: height * 0.00625, // 24
                                              ),
                                              Container(
                                                width: width * 0.39815, // 860
                                                margin: EdgeInsets.only(
                                                    top: height * 0.014167), // 40,40
                                                child: Image.network(
                                                  chatBoxController.imageUrl,
                                                  fit: BoxFit.fitWidth,
                                                ),
                                              ),
                                            ],
                                          )
                                        : Container(
                                            margin: EdgeInsets.only(
                                              top: height * 0.01667, // 64
                                            ),
                                            width: width * 0.3518519, // 760
                                            height: height * 0.0625, //240
                                            constraints: BoxConstraints(
                                                minHeight: height * 0.0625),
                                            child: (ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      width * 0.007407), // 16
                                              child: BackdropFilter(
                                                filter: ImageFilter.blur(
                                                  sigmaX: width * 0.00833, // 18
                                                  sigmaY: width * 0.00833, // 00833
                                                ),
                                                child: Center(
                                                  child: Container(
                                                    padding: EdgeInsets.only(
                                                        top: height * 0.00781215,
                                                        bottom: height * 0.00781215), // 30
                                                    width: width * 0.3518519, // 760
                                                    decoration: BoxDecoration(
                                                      color: Colors.black.withOpacity(0.4),
                                                    ),
                                                    child: Center(
                                                      child: SizedBox(
                                                        height: height * 0.046875,
                                                        child: Image.network(
                                                          chatBoxController
                                                              .imageUrl,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            )),
                                          ))
                                    : Container(),
                                if (listWidget != null)
                                  Column(
                                    children: [
                                      Wrap(children: listWidget),
                                    ],
                                  ),
                                Container(
                                  width: width * 0.39815,
                                  child: FirstButton(
                                    height: height,
                                    width: width,
                                  ),
                                )
                              ],
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class SideButton extends StatefulWidget {
  final String text;

  const SideButton({Key? key, required this.text}) : super(key: key);

  @override
  _SideButtonState createState() => _SideButtonState();
}

class _SideButtonState extends State<SideButton> {
  Color buttonColor = Colors.black.withOpacity(0.4);

  @override
  Widget build(BuildContext context) {
    ChatBoxController chatBoxController = Get.put(ChatBoxController());
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    return GetBuilder<ChatBoxController>(builder: (controller) {
      return GestureDetector(
        child: Container(
          margin: EdgeInsets.only(
              top: height * 0.014167, right: width * 0.01852), // 40,40
          child: GestureDetector(
            onTapDown: (TapDownDetails t) {
              setState(() {
                buttonColor = Colors.black.withOpacity(0.7);
              });
            },
            onTapUp: (TapUpDetails tapUpDetails) async {
              setState(() {
                buttonColor = Colors.black.withOpacity(0.4);
              });
              if (chatBoxController.isRenewed) {
                chatBoxController.updateRenew(false);
                await Repository.sendTextReq(widget.text);
              }
            },
            child: ClipRRect(
              borderRadius: BorderRadius.circular(width * 0.007407), // 16
              child: BackdropFilter(
                filter: ImageFilter.blur(
                  sigmaX: width * 0.00462963, // 10
                  sigmaY: width * 0.00462963, // 10
                ),
                child: Container(
                  padding: EdgeInsets.fromLTRB(
                      width * 0.02222, // 48
                      height * 0.009375, // 36
                      width * 0.02222, // 48
                      height * 0.00833), // 32
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: width * 0.000462963, // 1
                      color: const Color.fromRGBO(0, 0, 0, 0.25),
                    ),
                    color: buttonColor,
                    boxShadow: const [
                      BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.16),
                        blurRadius: 24,
                        offset: Offset(12, 12),
                      )
                    ],
                  ),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        widget.text,
                        style: TextStyle(
                          fontFamily: 'naumSquare',
                          fontWeight: FontWeight.w400,
                          color: Colors.white,
                          fontSize: width * 0.02407,
                          // 52
                          height: 1,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      );
    });
  }
}
