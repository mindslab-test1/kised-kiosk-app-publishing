import 'package:flutter/material.dart';
import 'package:kised/controllers/chat_box_controller.dart';
import 'package:kised/widgets/menus/chat_menu.dart';
import 'package:kised/widgets/menus/choice_menu.dart';
import 'package:kised/widgets/all_schedule.dart';
import 'package:kised/widgets/inside_img_view.dart';
import 'package:get/get.dart';

class MainMenu extends StatefulWidget {
  const MainMenu() : super();

  @override
  _MainMenuState createState() => _MainMenuState();
}

class _MainMenuState extends State<MainMenu> {

  @override
  Widget build(BuildContext context) {
    ChatBoxController chatBoxController = Get.put(ChatBoxController());
    print(chatBoxController.isChatMenu);
    return GetBuilder<ChatBoxController>(builder: (_) {
      return chatBoxController.isChatMenu
          // ? ImgInsideView()
          // ? AllSchedule()
          ? ChoiceMenu()
          : ChatMenu();
    });
  }
}
