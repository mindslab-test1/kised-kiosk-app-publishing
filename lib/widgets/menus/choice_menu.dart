import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kised/controllers/chat_box_controller.dart';
import 'package:kised/repository/repository.dart';
import 'dart:ui';
import 'package:get/get.dart';

import '../first_button.dart';

class ChoiceMenu extends StatefulWidget {
  const ChoiceMenu({Key? key}) : super(key: key);

  @override
  _ChoiceMenuState createState() => _ChoiceMenuState();
}

class _ChoiceMenuState extends State<ChoiceMenu> {
  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    return Container(
      padding: EdgeInsets.only(right: width * 0.03704),
      width: MediaQuery
          .of(context)
          .size
          .width,
      height: MediaQuery
          .of(context)
          .size
          .height,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                SizedBox(
                  height: height * 0.2401,
                ),
                Container(
                  width: width * 0.4134259,
                  child: Stack(
                    children: [
                      SizedBox(
                        width: width * 0.39815,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: width * 0.39815,
                              child: Text(
                                '어떠한 안내를 원하시나요?',
                                style: TextStyle(
                                  fontFamily: 'notoSansKR',
                                  fontWeight: FontWeight.w400,
                                  color: Colors.white,
                                  fontSize: width * 0.02407,// 52
                                  letterSpacing: -1.3,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: height * 0.00625, // 24
                            ),
                            SideButton(img: 'assets/img/ico_rocket_64px.png',
                                text: '비즈툴이란?',),
                            SideButton(img: 'assets/img/ico_rocket_64px.png',
                                text: '청소년 비즈쿨의 역사',),
                            SideButton(img: 'assets/img/ico_rocket_64px.png',
                                text: '프로그램 안내',),
                            SideButton(img: 'assets/img/ico_rocket_64px.png',
                                text: '주요 부대행사',),
                            SideButton(img: 'assets/img/ico_rocket_64px.png',
                                text: '주차 안내',),
                            SideButton(img: 'assets/img/ico_rocket_64px.png',
                                text: '제페토 월드에서 만나는\n2021 청소년 비즈쿨\n페스티벌',),
                            SideButton(img: "assets/img/ico_rocket_64px.png",
                                text: '오프라인 부대행사',),
                            FirstButton(height: height, width: width,)
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class SideButton extends StatefulWidget {
  final String img;
  final String text;

  const SideButton(
      {Key? key, required this.img, required this.text})
      : super(key: key);

  @override
  _SideButtonState createState() => _SideButtonState();
}

class _SideButtonState extends State<SideButton> {
  Color buttonColor = Colors.black.withOpacity(0.4);

  @override
  Widget build(BuildContext context) {
    ChatBoxController chatBoxController = Get.put(ChatBoxController());
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    return GetBuilder<ChatBoxController>(
        builder: (controller) {
      return Container(
        margin: EdgeInsets.only(top: height * 0.014167),
        // 40
        child: GestureDetector(
            onTapDown: (TapDownDetails t) {
              setState(() {
                buttonColor = Colors.black.withOpacity(0.7);
              });
            },
            onTapUp: (TapUpDetails tapUpDetails) async {
              setState(() {
                buttonColor = Colors.black.withOpacity(0.4);
              });
              if (chatBoxController.isRenewed) {
                chatBoxController.updateRenew(false);
                await Repository.sendTextReq(widget.text);
                chatBoxController.updateMenu(false);
              }
            },
            child: ClipRRect(
              borderRadius:
              BorderRadius.circular(width * 0.007407),
              child: BackdropFilter(
                filter: ImageFilter.blur(
                  sigmaX: width * 0.00462963,
                  sigmaY: width * 0.00462963,
                ),
                child: Container(
                  padding: EdgeInsets.fromLTRB(
                      width * 0.02963,  // 64
                      height * 0.0117875,
                      0,
                      height * 0.0117875),
                  // height: height * 0.04167,
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: width * 0.000462963,
                      color: Color.fromRGBO(0, 0, 0, 0.25),
                    ),
                    color: buttonColor,
                  ),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: width * 0.02963,
                        height: width * 0.02963,
                        child: Image.asset(
                          widget.img,
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(width: width * 0.02222), // 48
                      Flexible(
                        child: Container(
                          child: Text(
                            widget.text,
                            // softWrap: true,
                            textAlign: TextAlign.left,
                            overflow: TextOverflow.visible,
                            // overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              fontFamily: 'naumSquare',
                              fontWeight: FontWeight.w400,
                              color: Colors.white,
                              fontSize: width * 0.02963,
                              height: 1.1,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )
        ),
      );
    });
  }
}

